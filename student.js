import { Person } from "./person.js";
class Student extends Person {
    constructor(name, age, gender, standard, collegeName, grade) {
        super(name, age, gender);
        this.standard = standard;
        this.collegeName = collegeName;
        this.grade = grade;
    }

    getStudentInfo() {
        return `${this.getPersonInfo()}, Standard: ${this.standard}, College Name: ${this.collegeName}, Grade: ${this.grade}`;
    }

    getGrade() {
        return this.grade;
    }

    setGrade(newGrade) {
        this.grade = newGrade;
    }
}

export {Student}