class Person {
    constructor(name, age, gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    getPersonInfo() {
        return `Name: ${this.name}, Age: ${this.age}, Gender: ${this.gender}`;
    }
}

export {Person}