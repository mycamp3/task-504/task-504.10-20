import { Employee } from "./employee.js";
import { Person } from "./person.js";
import { Student } from "./student.js";

console.log("Person Class");
const person1 = new Person('John Doe', 30, 'Male');
const person2 = new Person('Jane Smith', 25, 'Female');
const person3 = new Person('Alex Johnson', 40, 'Non-binary');

console.log(person1.getPersonInfo());
console.log(person2.getPersonInfo());
console.log(person3.getPersonInfo());

// Kiểm tra xem các đối tượng có phải là thể hiện của lớp Person hay không
console.log(person1 instanceof Person); // true
console.log(person2 instanceof Person); // true
console.log(person3 instanceof Person); // true


console.log("Student SubClass");
const student1 = new Student('John Doe', 20, 'Male', 'Sophomore', 'ABC University', 'A');
const student2 = new Student('Jane Smith', 22, 'Female', 'Senior', 'XYZ College', 'B');
const student3 = new Student('Alex Johnson', 21, 'Non-binary', 'Junior', 'LMN Institute', 'A+');

console.log(student1.getGrade()); // A
student1.setGrade('A+');
console.log(student1.getGrade()); // A+

console.log(student1 instanceof Student); // true
console.log(student1 instanceof Person); // true
console.log(student2 instanceof Student); // true
console.log(student2 instanceof Person); // true
console.log(student3 instanceof Student); // true
console.log(student3 instanceof Person); // true

console.log("Employee SubClass");
const employee1 = new Employee('John Doe', 20, 'Male', 'Sophia', '$400', 'intern');
const employee2 = new Employee('Jane Smith', 22, 'Female', 'Alex', '$500', 'intern');
const employee3 = new Employee('Alex Johnson', 21, 'Non-binary', 'David', '$1000', 'junior');

console.log(employee1.getSalary()); 
employee1.setSalary('$800');
console.log(employee1.getSalary());

