import { Person} from "./person.js"
class Employee extends Person {
    constructor(name, age, gender, employer, salary, position) {
        super(name, age, gender);
        this.employer = employer;
        this.salary = salary;
        this.position = position;
    }

    getEmployeeInfo() {
        return `${this.getPersonInfo()}, Employer: ${this.employer}, Salary: ${this.salary}, Position: ${this.position}`;
    }

    getSalary() {
        return this.salary;
    }

    setSalary(newSalary) {
        this.salary = newSalary;
    }

    getPosition() {
        return this.position;
    }

    setPosition(newPosition) {
        this.position = newPosition;
    }
}

export { Employee}